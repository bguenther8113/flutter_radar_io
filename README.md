
# IF YOU ARE AN IOS DEVELOPER, THIS PLUGIN NEEDS YOUR SUPPORT!

The iOS implementation of this public is incomplete (e.g. it is missing the possibility of setting a callback from dart)

Your contribution would be very appreciated!
  
# :arrow_up: Flutter Radar.io    
 This *unofficial* plugin allows  to use the Radar.io SDK in your Flutter mobile app on iOS and Android. It is still in its early stage but it already allows to use most of the features of the Radar.io SDK.  
    
  ## Development progress  
  
  
 - User identification ✔️   
- Foreground tracking ✔️  
*Callbacks are in Android only*
     
- Background tracking ✔️  
 - Configuration ✔️  
  *Currently you can only choose one of the 3  
   predefined configurations*  
 - Event verification ✖️  
 - Location search ✖️  
 - Geocoding ✖️  
 - Distance ✖️  
 - Permission request  ✖️  
  *Not a priority as  
   there are already many great plugins that perform this*  
  ## Setup    
 ### Android  
Add the following permissions to your `AndroidManifest.xml` in `android/app/src/main/AndroidManifest.xml`:

```
<uses-permission android:name="android.permission.INTERNET"/>    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />    
        <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />    
        <uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION" />    
        <uses-permission android:name="android.permission.ACCESS_ACCESS_NETWORK_STATE" />    
        <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
```

  
### iOS  
This plugin requires at least iOS 10.0.  
  
Edit your first line of your `PodFile` like this:  
  
  
`platform :ios, '10.0'`

Then add the following keys to your `Info.plist` file. Make sure to replace the placeholder descriptions with some meaningful explanation related to why you are requesting these permissions.  
  

```
<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>       <string>Your iOS 11 and higher background location usage description goes here. e.g., "This app uses your location in the background to recommend places nearby."</string>    
            
           <key>NSLocationAlwaysUsageDescription</key>    
           <string>Your iOS 10 and lower background location usage description goes here. e.g., "This app uses your location in the background to recommend places nearby."</string>    
            
           <key>NSLocationWhenInUseUsageDescription</key>    
           <string>Your foreground location usage description goes here. e.g., "This app uses your location in the foreground to recommend places nearby."</string>    
           <key>UIBackgroundModes</key>    
           <array>    
             <string>fetch</string>    
             <string>location</string>    
           </array>  
```
  
  
### Requesting permissions  
Additionally, you will have to manage the location permission request. In the example app, you will see that we have used [permission_handler](https://pub.dev/packages/permission_handler) but feel free to use any other plugin you like. If you choose to go with permission [permission_handler](https://pub.dev/packages/permission_handler) make sure to follow their installation steps as well.  
  
## Usage  
You can check the example for a rather complete implementation and you can always check the  [Official docs](https://radar.io/documentation/sdk#android-initialize). 
