import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

typedef void RadarCallBack({
  @required String radarStatus,
  Map<dynamic, dynamic> location,
  List<Map<dynamic, dynamic>> radarEvents,
  Map<dynamic, dynamic> radarUser,
});

class FlutterRadarIo {
  static const String METHOD_INITIALIZE = "initialize";
  static const String METHOD_SET_USER_ID = "set_user_id";
  static const String METHOD_GET_USER_ID = "get_user_id";
  static const String METHOD_SET_METADATA = "set_metadata";
  static const String METHOD_GET_METADATA = "get_metadata";
  static const String METHOD_SET_DESCRIPTION = "set_description";
  static const String METHOD_GET_DESCRIPTION = "get_description";
  static const String METHOD_TRACK_ONCE = "track_once";
  static const String METHOD_START_TRACKING = "start_tracking";
  static const String METHOD_STOP_TRACKING = "stop_tracking";
  static const String METHOD_IS_TRACKING = "is_tracking";
  static const String METHOD_SET_LOG_LEVEL = "set_log_level";

  static const String TRACKING_PRESET_EFFICIENT = "efficient";
  static const String TRACKING_PRESET_RESPONSIVE = "responsive";
  static const String TRACKING_PRESET_CONTINUOUS = "continuous";

  static const String LOG_LEVEL_ERROR = "log_level_error";
  static const String LOG_LEVEL_NONE = "log_level_none";
  static const String LOG_LEVEL_WARNING = "log_level_warning";
  static const String LOG_LEVEL_INFO = "log_level_info";
  static const String LOG_LEVEL_DEBUG = "log_level_debug";

  static const MethodChannel _channel = const MethodChannel('flutter_radar_io');

  static RadarCallBack _callBack;

  static Future<bool> initialize({
    @required String publishableKey,
  }) async {
    final Map<String, dynamic> params = <String, dynamic>{
      'publishableKey': publishableKey
    };
    final bool result = await _channel.invokeMethod(METHOD_INITIALIZE, params);
    _channel.setMethodCallHandler(_methodCallHandler);
    return result;
  }

  static Future<String> setUserId({
    @required String userId,
  }) async {
    final Map<String, dynamic> params = <String, dynamic>{'userId': userId};
    final String result =
        await _channel.invokeMethod(METHOD_SET_USER_ID, params);
    return result;
  }

  static Future<String> getUserId() async {
    final String result = await _channel.invokeMethod(METHOD_GET_USER_ID);
    return result;
  }

  static Future<bool> isTracking() async {
    final bool result = await _channel.invokeMethod(METHOD_IS_TRACKING);
    return result;
  }

  static Future<bool> trackOnce({RadarCallBack callBack}) async {
    _callBack = callBack;
    final bool result = await _channel.invokeMethod(METHOD_TRACK_ONCE);
    return result;
  }

  static Future<bool> startTracking({
    @required String preset,
  }) async {
    final Map<String, dynamic> params = <String, dynamic>{'preset': preset};
    final bool result =
        await _channel.invokeMethod(METHOD_START_TRACKING, params);
    return result;
  }

  static Future<bool> stopTracking() async {
    final bool result = await _channel.invokeMethod(METHOD_STOP_TRACKING);
    return result;
  }

  static Future<String> setDescription({
    @required String description,
  }) async {
    final Map<String, dynamic> params = <String, dynamic>{
      'description': description
    };
    final String result =
        await _channel.invokeMethod(METHOD_SET_DESCRIPTION, params);
    return result;
  }

  static Future<String> getDescription() async {
    final String result = await _channel.invokeMethod(METHOD_GET_DESCRIPTION);
    return result;
  }

  static Future<bool> setMetadata({
    @required Map<String, dynamic> metadata,
  }) async {
    final bool result =
        await _channel.invokeMethod(METHOD_SET_DESCRIPTION, metadata);
    return result;
  }

  static Future<bool> setLogLevel({
    @required String logLevel,
  }) async {
    final Map<String, dynamic> params = <String, dynamic>{'logLevel': logLevel};
    final bool result =
        await _channel.invokeMethod(METHOD_SET_LOG_LEVEL, params);
    return result;
  }

  static Future<void> _methodCallHandler(MethodCall call) async {
    switch (call.method) {
      case 'callback':
        _callBack(
          radarStatus: call.arguments['radarStatus'],
          location: call.arguments['location'],
          radarEvents: call.arguments['radarEvents'],
          radarUser: call.arguments['radarUser'],
        );
        break;
      default:
        print('This normally shouldn\'t happen.');
    }
  }
}
